
using UnityEngine;
using System.Collections;
 
public class Floater : MonoBehaviour {

    public float degreesPerSecond = 15.0f;
    public float amplitude = 0.5f;
    public float frequency = 1f;
    
    Vector3 _posOffset = Vector3.zero;
    Vector3 _tempPos = Vector3.zero;

    public bool Animate;

    void Start () {
        _posOffset = transform.localPosition;
    }
    
    void Update () {

        if (Animate)
        {
            transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);
            
            _tempPos = _posOffset;
            _tempPos.y += Mathf.Sin (Time.fixedTime * Mathf.PI * frequency) * amplitude;
 
            transform.localPosition = _tempPos;
        }
    }
}