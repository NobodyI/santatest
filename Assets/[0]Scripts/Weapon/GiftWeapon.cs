using System;
using MoreMountains.Feedbacks;
using Sirenix.Utilities;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class GiftWeapon : MonoBehaviour
{
    [SerializeField] private GiftBullet bullet;
    [SerializeField] private Transform missileTransform;
    [SerializeField] private LayerMask shootableLayers;
    [SerializeField] private float shootMaxDistance = 10f;
    [SerializeField] private float delayToNextShoot = 0.5f;
    [SerializeField] private MMFeedbacks shootFeedback;

    private RaycastHit _hit;
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            InvokeRepeating(nameof(Shoot), delayToNextShoot, delayToNextShoot);
        }

        if (Input.GetMouseButtonUp(0))
        {
            CancelInvoke(nameof(Shoot));
        }
    }

    private void OnDisable()
    {
        CancelInvoke(nameof(Shoot));
    }

    private void Shoot()
    {
        shootFeedback.PlayFeedbacks();
        
        GiftBullet giftBullet = Instantiate(bullet, missileTransform.position, Quaternion.identity);
        giftBullet.Initialize();
        
        if (Physics.Raycast(missileTransform.position, missileTransform.forward, out _hit, shootMaxDistance, shootableLayers))
        {
            IHitable hitableObject = null;
            if (_hit.collider.gameObject.TryGetComponent(out hitableObject))
            {
                giftBullet.HitToObject(hitableObject);
            }
            else
            {
                giftBullet.HitToEmptySpace(missileTransform);
            }
        }
        else
        {
            giftBullet.HitToEmptySpace(missileTransform);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(missileTransform.position, missileTransform.position + (missileTransform.forward*shootMaxDistance));
    }
}
