using DG.Tweening;
using MoreMountains.Feedbacks;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GiftBullet : MonoBehaviour
{
    [SerializeField] private float power;
    [SerializeField] private AnimationCurve movementAnimationCurve;
    [SerializeField] private float animationDuration = 1f;
    [SerializeField] private MMFeedbacks initializeFeedbacks;

    private Rigidbody _rigidbody;
    private float _expiredSeconds;
    private float _progress;
    private float _startPosition;
    
    public void Initialize()
    {
        _rigidbody = GetComponent<Rigidbody>();
        initializeFeedbacks.PlayFeedbacks();
    }

    public void HitToObject(IHitable hitableObject)
    {
        _expiredSeconds = 0f;
        _progress = 0f;
        _startPosition = transform.position.y;

        transform.DOMove(hitableObject.HitPoint.position, animationDuration)
            .OnUpdate(MoveBulletAlongCurve)
            .OnComplete(() => RunHitAndDestroyBullet(hitableObject));
    }

    public void HitToEmptySpace(Transform missleTransform)
    {
        _rigidbody.isKinematic = false;
        _rigidbody.AddForce((missleTransform.forward + missleTransform.up) * power, ForceMode.Impulse);
        DestroyBullet();
    }

    private void RunHitAndDestroyBullet(IHitable hitableObject)
    {
        hitableObject.Hit();
        DestroyBullet(false);
    }
    
    private void MoveBulletAlongCurve()
    {
        if (_progress < 1)
        {
            _expiredSeconds += Time.deltaTime;
            _progress = _expiredSeconds / animationDuration;

            var position = transform.position;
            var yMove = _startPosition + movementAnimationCurve.Evaluate(_progress);
            position = new Vector3(position.x, yMove, position.z);
            transform.position = position;
        }
    }

    private void DestroyBullet(bool withDelay = true)
    {
        Destroy(gameObject, withDelay ? 5f : 0f);
    }
}
