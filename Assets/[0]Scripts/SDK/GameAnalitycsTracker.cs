using System;
using UnityEngine;
using GameAnalyticsSDK;

public class GameAnalitycsTracker : MonoBehaviour
{
    public struct BusinessEventProperties
    {
        public string currency; //Currency code in ISO 4217 format. Ex : 'USD'
        public int amount; //Amount in cents. Ex : 99 is 0.99
        public string itemType; //The type of the item. Ex : 'GoldPacks'
        public string itemId; //Specific item bought. Ex : '1000GoldPack'
        public string cartType; //The game location of the purchase. Max 10 unique values. Ex : 'EndOfLevel'

        /// <summary>
        /// Android Receipt Contents (JSON format): 
        /// {
        ///"orderId": "<order_id>",
        ///"packageName": "<package_name>",
        ///"productId": "<product_id>",
        ///"purchaseTime": 1484080095335,
        ///"purchaseState": 0,
        ///"purchaseToken": "<purchase_token>"
        ///}           
        /// </summary>

        public BusinessEventProperties(string _currancy, int _amount, string _itemType, string _itemId, string _cartType)
        {
            currency = _currancy;
            amount = _amount;
            itemType = _itemType;
            itemId = _itemId;
            cartType = _cartType;
        }
    }

    public struct AdEventProperties
    {
        public GAAdAction adAction; //Current ad action 
        public GAAdType adType; //current ad type
        public string adSdkName; // ad ask name. Ex : 'admob' , 'unityads' , 'ironsource' , 'applovin'
        public string adPlacement; //placement id

        public bool isError; //Optional, by default FALSE. when ad is not being able to show change the flag in TRUE
        public GAAdError noAdReason; //Optional. Used to track the reason for not being able to show an ad when needed (for example no fill).

        public AdEventProperties(GAAdAction _adAction, GAAdType _adType, string _adSdkName, string _adPlacement, bool _isError = false, GAAdError _noAdError = GAAdError.Offline)
        {
            adAction = _adAction;
            adType = _adType;
            adSdkName = _adSdkName;
            adPlacement = _adPlacement;

            isError = _isError;
            noAdReason = _noAdError;
        }
    }

    public bool Initialized { get; private set; }

    public bool manualInitialization = false;

    private string currentRewardedVideoPlacement;

    public void Start()
    {
        currentRewardedVideoPlacement = null;
        
        if(manualInitialization == false)
        {
            InitializeGameAnalitycs();
        }
        
        DontDestroyOnLoad(gameObject);
    }

    public void InitializeGameAnalitycs()
    {
        try
        {
            GameAnalytics.Initialize();
            Initialized = true;
        }
        catch (Exception e)
        {
            Debug.LogError("GAME_ANALITYCS ERROR -> InitializeGameAnalitycs() -> " + e);
            Initialized = false;
        }
    }

    public void SendBusinessEvent(BusinessEventProperties eventProperties)
    {
        if (CheckInitiallization() == false)
        {
            return;
        }

        GameAnalytics.NewBusinessEvent(eventProperties.currency, eventProperties.amount, eventProperties.itemType, eventProperties.itemId, eventProperties.cartType);
    }

    public void SendAdEvent(AdEventProperties eventProperties)
    {
        if (CheckInitiallization() == false)
        {
            return;
        }

        if (eventProperties.isError)
        {
            GameAnalytics.NewAdEvent(eventProperties.adAction, eventProperties.adType, eventProperties.adSdkName, eventProperties.adPlacement, eventProperties.noAdReason);
        }
        else
        {
            GameAnalytics.NewAdEvent(eventProperties.adAction, eventProperties.adType, eventProperties.adSdkName, eventProperties.adPlacement);
        }
    }

    public void SendAdEventWithTimeTracking(AdEventProperties eventProperties)
    {
        if (CheckInitiallization() == false)
        {
            return;
        }

        GameAnalytics.NewAdEvent(eventProperties.adAction, eventProperties.adType, eventProperties.adSdkName, eventProperties.adPlacement, GetElapsedTime());
    }

    public void StartTrakingTime(string rewardedVideoPlacement)
    {
        if (string.IsNullOrEmpty(currentRewardedVideoPlacement) == false)
        {
            GameAnalytics.StopTimer(currentRewardedVideoPlacement);
        }

        currentRewardedVideoPlacement = rewardedVideoPlacement;
        GameAnalytics.StartTimer(currentRewardedVideoPlacement);
    }

    public long GetElapsedTime()
    {
        if (CheckInitiallization() == false)
        {
            return 0;
        }

        if (string.IsNullOrEmpty(currentRewardedVideoPlacement) == false)
        {
            currentRewardedVideoPlacement = null;
            return GameAnalytics.StopTimer(currentRewardedVideoPlacement);
        }

        return 0;
    }

    private bool CheckInitiallization()
    {
        if (Initialized == false)
        {
            Debug.LogError("GAME_ANALITYCS ERROR -> GAME_ANALITYCS IS NOT INITIALIZED");
        }

        return Initialized;
    }

    void OnApplicationPause(bool paused)
    {
        if (string.IsNullOrEmpty(currentRewardedVideoPlacement))
        {
            return;
        }

        if (paused)
        {
            GameAnalytics.PauseTimer(currentRewardedVideoPlacement);
        }
        else
        {
            GameAnalytics.ResumeTimer(currentRewardedVideoPlacement);
        }
    }
}
