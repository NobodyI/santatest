
using UnityEngine;

public interface IHitable
{
    public Transform HitPoint
    {
        get;
    }

    public void Hit();
}
