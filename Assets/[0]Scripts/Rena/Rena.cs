using System;
using Sirenix.OdinInspector;
using UnityEditor.Animations;
using UnityEngine;

[RequireComponent(typeof(Animator))] [RequireComponent(typeof(Floater))]
public class Rena : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] moveParticles;

    private Animator _animator;
    private Floater _floater;
    private readonly int Move = Animator.StringToHash("move");

    [Title("DEBUG BUTTONS", "", TitleAlignments.Centered)]
    [Button] [GUIColor(0, 1, 0)]
    private void EnableMovementTest()
    {
        RunMovement();
    }
    
    [Button] [GUIColor(0, 1, 0)]
    private void DisableMovementTest()
    {
        StopMovement();
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _floater = GetComponent<Floater>();
    }

    public void StopMovement()
    {
        ChangeMovementState(false);
    }

    public void RunMovement()
    {
        ChangeMovementState(true);
    }

    private void ChangeMovementState(bool state)
    {
        _animator.SetBool(Move, state);
        _floater.Animate = state;
        ChangeMovementParticlesState(state);
    }

    private void ChangeMovementParticlesState(bool state)
    {
        foreach (var particle in moveParticles)
        {
            particle.loop = state;
            
            if (state)
            {
                particle.Play();
            }
        }
    }
}
