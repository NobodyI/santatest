using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class QuestsBehaviour : MonoBehaviour
{
    [System.Serializable]
    public struct StageProperties
    {
        public Transform questPlace;
        public PeoplesHolder peoplesHolder;
    }

    [Title("STAGE CONFIGS", "", TitleAlignments.Centered)]
    [SerializeField] private List<StageProperties> stages = new List<StageProperties>();

    [Space] [Title("REFERENCES", "", TitleAlignments.Centered)]
    [SerializeField] private SantaStateController santaStateController;
    
    private int _stagesCount;
    private int _currentStage;
    private EventsHolder _eventsHolder => EventsHolder.Instance;

    private void Awake()
    {
        _stagesCount = stages.Count - 1;
        _currentStage = 0;
    }

    private void Start()
    {
        GoToTheNextStage();
    }

    private void OnEnable()
    {
        _eventsHolder.CompleteStageEvent.AddListener(GoToTheNextStage);
        _eventsHolder.BeginStageEvent.AddListener(BeginStage);
    }

    private void OnDisable()
    {
        _eventsHolder.CompleteStageEvent.RemoveListener(GoToTheNextStage);
        _eventsHolder.BeginStageEvent.RemoveListener(BeginStage);
    }

    private void GoToTheNextStage()
    {
        if (_currentStage > _stagesCount)
        {
            _eventsHolder.WinEvent.Invoke();
            return;
        }
        
        stages[_currentStage].peoplesHolder.Initialize();
        santaStateController.ToggleToMove(stages[_currentStage]);
        
        _currentStage += 1;
    }

    private void BeginStage()
    {
        santaStateController.ToggleToShoot();
    }
}
