using System;
using DG.Tweening;
using UnityEngine;

public class MainInput : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float targetSpeed;
    [SerializeField] private float mouseSensitivity;
    [SerializeField] private float touchSensitivity;

    [SerializeField] private Vector2 minClampedRotation;
    [SerializeField] private Vector2 maxClampedRotation;

    [SerializeField] private Vector3 dieCameraRotation;
    
    private Vector2 _mouseInput;
    
    private Vector2 _inputRotation;
    private Vector2 _startRotation;

    private void Start()
    {
        _startRotation.x = target.localEulerAngles.y;
        _startRotation.y = target.localEulerAngles.x;
        _inputRotation = _startRotation;
    }

    private void FixedUpdate()
    {
        cameraTransform.DORotate(new Vector3(target.eulerAngles.x, target.eulerAngles.y, 0), targetSpeed * Time.deltaTime);
        target.localRotation = Quaternion.Euler(_inputRotation.y, _inputRotation.x, target.localRotation.z);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            CameraViewInput();
        }
    }

    private void CameraViewInput()
    {
        if (Input.touchCount > 0)
        {
            _mouseInput.x = -Input.touches[0].deltaPosition.x * touchSensitivity * Time.deltaTime;
            _mouseInput.y = Input.touches[0].deltaPosition.y * touchSensitivity * Time.deltaTime;
        }
        else
        {
            _mouseInput.x = -Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            _mouseInput.y = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        }
        
        _inputRotation.x -= _mouseInput.x;
        _inputRotation.y -= _mouseInput.y;

        _inputRotation.x = Mathf.Clamp(_inputRotation.x, _startRotation.x + minClampedRotation.x, _startRotation.x + maxClampedRotation.x);
        _inputRotation.y = Mathf.Clamp(_inputRotation.y, _startRotation.y + minClampedRotation.y, _startRotation.y + maxClampedRotation.y);
    }

    private void BlockCameraAfterPlayerDied()
    {
        cameraTransform.transform.DORotate(dieCameraRotation, 0.5f);
    }
}
