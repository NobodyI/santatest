using System;
using DG.Tweening;
using UnityEngine;

public class SledMovement : MonoBehaviour
{
    [SerializeField] private float speed = 10f;

    private EventsHolder _eventsHolder => EventsHolder.Instance;
    
    public void MoveToPosition(Transform target)
    {
        transform.DOMove(target.position, speed).OnComplete(StopMoving);
    }

    private void StopMoving()
    {
        _eventsHolder.BeginStageEvent.Invoke();
    }
}
