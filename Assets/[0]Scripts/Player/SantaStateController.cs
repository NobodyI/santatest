using System;
using System.Collections.Generic;
using DG.Tweening;
using GameAnalyticsSDK.Setup;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(SledMovement))]
public class SantaStateController : MonoBehaviour
{
    [SerializeField] private Transform renasParentTransform;
    [SerializeField] private Floater sledFloater;
    [SerializeField] private GameObject fps_Santa;
    [SerializeField] private GameObject standartSanta;

    private SledMovement _sledMovement;
    private Rena[] _renasList;

    [Title("DEBUG BUTTONS", "", TitleAlignments.Centered)]
    [Button] [GUIColor(0, 1, 0)]
    private void ToggleToMoveTest()
    {
        ToggleToMove(new QuestsBehaviour.StageProperties());
    }
    
    [Button] [GUIColor(0, 1, 0)]
    private void ToggleToShotTest()
    {
        ToggleToShoot();
    }
    
    private void Awake()
    {
        _sledMovement = GetComponent<SledMovement>();
        _renasList = renasParentTransform.GetComponentsInChildren<Rena>();
    }

    public void ToggleToMove(QuestsBehaviour.StageProperties properties)
    {
        ChangeSantaVisualizationMode(true);
        _sledMovement.MoveToPosition(properties.questPlace);
    }

    public void ToggleToShoot()
    {
        ChangeSantaVisualizationMode(false);
    }

    //true - to move, false - to stop and shoot
    private void ChangeSantaVisualizationMode(bool state)
    {
        foreach (var rena in _renasList)
        {
            if (state)
            {
                rena.RunMovement();
            }
            else
            {
                rena.StopMovement();
            }
        }

        sledFloater.Animate = state;
        
        fps_Santa.SetActive(!state);
        DOVirtual.DelayedCall(state ? 0.1f : 0.4f, () =>
        {
            standartSanta.SetActive(state);
        });
    }
}
