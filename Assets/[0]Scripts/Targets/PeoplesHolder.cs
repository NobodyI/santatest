using System.Collections.Generic;
using UnityEngine;

public class PeoplesHolder : MonoBehaviour
{
    private List<People> _peoples = new List<People>();

    private EventsHolder _eventsHolder => EventsHolder.Instance;
    
    public void Initialize()
    {
        _peoples.AddRange(GetComponentsInChildren<People>());
        foreach (var people in _peoples)
        {
            people.hitEvent += OnEnemyHitted;
        }
    }

    private void RemovePeople(People people)
    {
        people.hitEvent -= OnEnemyHitted;
        _peoples.Remove(people);

        if (_peoples.Count == 0)
        {
            _eventsHolder.CompleteStageEvent.Invoke();
        }
    }

    private void OnEnemyHitted(People people)
    {
        RemovePeople(people);
    }
}
