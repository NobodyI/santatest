using System;
using System.Runtime.CompilerServices;
using MoreMountains.Feedbacks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class People : MonoBehaviour, IHitable
{
    public event Action<People> hitEvent;
    public Transform HitPoint => hitPoint;
    [SerializeField] private Transform hitPoint;
    [SerializeField] private MMFeedbacks hitFeedbacks;

    private Rigidbody _rigidbody;
    private bool _defeated = false;
    
    public void Hit()
    {
        if (_defeated)
        {
            return;
        }
        
        hitFeedbacks.PlayFeedbacks();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = false;
        _defeated = true;
        hitEvent?.Invoke(this);
    }
}
